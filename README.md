# Web Development with ASP.Net Core - Western Technical College #

In this repository you will find most my coursework from Web 
Development with ASP.Net, a 2018 Spring course at Western Technical College in La Crosse, Wisconsin. This course has the following description (which needs to be updated, but I'll explain more details below):
"This programming course teaches the student how to create dynamic web content and covers advanced object oriented programming principles. The course utilizes SQL and continues building on HTML and .NET skills. Through the use of a database (Microsoft SQL Server), a web programming language (ASP.Net)and a web server, the student will learn how to create database driven web sites."
Honestly, that is the description from the course syllabus, and I don't believe it has been updated in quite a while. The big thing that this description fails to mention is the fact the focus of the course is now on Microsoft's open source version of ASP.Net called ASP.Net Core. The design of the programs in this course are also strucuted using the Model View Controller (MVC) design pattern.

### Labs ###

* Lab 01 - [CD Tracker](./Lab_01 - CD Tracker)
* Lab 02 - [C# Language Features](./Lab_02 - Language Features)
* Lab 03 - [Razor Essentials](./Lab_03 - Razor Essentials)
* Lab 05 - [Unit Testing](./Lab_05 - Unit Testing)
* Lab 05 - [xUnitTests](./Lab_05 - xUnitTests
* Lab 06 - [EF Core Intro](./Lab_06 - EF Core Intro)

### Course Competencies ###

1. Examine advanced object-oriented program principles as implemented in Visual Basic.
2. Use external files in a Visual Basic program.
3. Deploy a Visual Basic Client Application.
4. Examine philosophical assumptions relative to ASP .Net.
5. Utilize server and validation controls in an ASP .Net application.
6. Implement ASP .Net web pages that properly manage state.
7. Utilize built-in constructs to manage web page look and navigation.
8. Implement database controls in ASP .Net.
9. Explore security issues in ASP .Net web sites.
10. Deploy a web-based application that uses ASP .Net
