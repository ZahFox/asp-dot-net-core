﻿using System;
using System.Collections.Generic;
using System.Text;
using Lab_05___Unit_Testing.Models;
using Lab_05___Unit_Testing.Controllers;
using Xunit;
using Microsoft.AspNetCore.Mvc;

namespace Lab_05___xUnitTests
{
    public class HomeControllerTest
    {
        [Fact]
        public void InitialIndex()
        {
            // Arrange
            HomeController controller = new HomeController();
            Tuple<IEnumerable<CD>, IEnumerable<Product>> one = Tuple.Create(Portfolio.CDs, Profile.Cart);

            // Act
            ViewResult result = (ViewResult)controller.Index();
            Tuple<IEnumerable<CD>, IEnumerable<Product>> response = (Tuple<IEnumerable<CD>, IEnumerable<Product>>)result?.ViewData.Model;

            // Assert
            Assert.Equal(one.Item2, response.Item2,
                Comparer.Get<Product>((p1, p2) => p1.Equals(p2)));

            Assert.Equal(one.Item1, response.Item1,
                Comparer.Get<CD>((c1, c2) => c1.Equals(c2)));
        }

        [Fact]
        public void AddingOnlyCDs()
        {
            // Arrange
            HomeController controller = new HomeController();
            Tuple<IEnumerable<CD>, IEnumerable<Product>> expected = Tuple.Create(TestCDPortfolio.GetTestData(), Profile.Cart);

            // Act
            foreach( CD cd in TestCDPortfolio.GetTestData())
            {
                controller.NewCD(cd);
            }
            ViewResult result = (ViewResult)controller.Index();
            Tuple<IEnumerable<CD>, IEnumerable<Product>> response = (Tuple<IEnumerable<CD>, IEnumerable<Product>>)result?.ViewData.Model;

            // Assert
            Assert.Equal(expected.Item2, response.Item2,
                Comparer.Get<Product>( (p1, p2) => p1.Equals(p2)) );

            Assert.Equal(expected.Item1, response.Item1,
                Comparer.Get<CD>( (c1, c2) => c1.Equals(c2)) );
        }

        [Theory]
        [InlineData("Taco", 2.99, "Great Tasting!")]
        [InlineData("Burger", 12.929, "Wow This Is Great!")]
        [InlineData("Hat", 1222.99, "It Fits!!")]
        [InlineData("Carl", 21232.99, "Is Cool!")]
        [InlineData("Winston", 3122.9239, "Has Issues...")]
        public void AddingOnlyProducts(string name, decimal price, string description)
        {
            // Arrange
            HomeController controller = new HomeController();
            IEnumerable<CD> CDs = Portfolio.CDs;

            // Act
            controller.AddProduct( new Product { Name = name, Price = price, Description = description } );
            ViewResult result = (ViewResult)controller.Index();
            Tuple<IEnumerable<CD>, IEnumerable<Product>> response = (Tuple<IEnumerable<CD>, IEnumerable<Product>>)result?.ViewData.Model;
            Tuple<IEnumerable<CD>, IEnumerable<Product>> expected = Tuple.Create(CDs, Profile.Cart);

            // Assert
            Assert.Equal(expected.Item2, response.Item2,
                Comparer.Get<Product>( (p1, p2) => p1.Equals(p2) ));

            Assert.Equal(expected.Item1, response.Item1,
                Comparer.Get<CD>( (c1, c2) => c1.Equals(c2)) );
        }
    }
}
