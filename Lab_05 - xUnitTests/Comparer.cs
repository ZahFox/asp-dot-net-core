﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_05___xUnitTests
{
    public class Comparer
    {
        public static Comparer<T> Get<T>(Func<T, T, bool> func)
        {
            return new Comparer<T>(func);
        }
    }

    public class Comparer<U> : Comparer, IEqualityComparer<U>
    {

        private Func<U, U, bool> comparisonFunc;

        public Comparer(Func<U, U, bool> func)
        {
            this.comparisonFunc = func;
        }

        public bool Equals(U x, U y)
        {
            return comparisonFunc(x, y);
        }

        public int GetHashCode(U obj)
        {
            return obj.GetHashCode();
        }
    }
}
