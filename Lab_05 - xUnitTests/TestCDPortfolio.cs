﻿using System;
using Lab_05___Unit_Testing.Models;
using System.Collections.Generic;

namespace Lab_05___xUnitTests
{
    public class TestCDPortfolio
    {

        public static IEnumerable<CD> GetTestData () => new List<CD>
        {
                new CD{ Bank = CD.Banks.Altra_FCU, Months = 12, DepositAmount = 200M, PurchaseDate = new DateTime(2015, 12, 25), Rate = 12D },
                new CD{ Bank = CD.Banks.BMO_Harris, Months = 24, DepositAmount = 1200M, PurchaseDate = new DateTime(2015, 12, 25), Rate = 6D },
                new CD{ Bank = CD.Banks.Synchrony_Bank, Months = 36, DepositAmount = 250M, PurchaseDate = new DateTime(2015, 12, 25), Rate = 2.5D },
                new CD{ Bank = CD.Banks.Altra_FCU, Months = 69, DepositAmount = 60M, PurchaseDate = new DateTime(2015, 12, 25), Rate = 8.25D }
        };
    }
}
