﻿using System;
using Microsoft.AspNetCore.Mvc;
using Lab_05___Unit_Testing.Models;
using System.Linq;

namespace Lab_05___Unit_Testing.Controllers
{
    public class HomeController : Controller
    {
        private const int MAX_MONTHS_CD_DISPLAY = 5;

        public int GetMaxMonthCdDisplay => MAX_MONTHS_CD_DISPLAY;

        [HttpGet]
        public IActionResult Index () => View( Tuple.Create(Portfolio.CDs.Where(c => c.Months > MAX_MONTHS_CD_DISPLAY), Profile.Cart) );
 

        [HttpGet] public IActionResult AddProduct () => View( new Product() );
        

        [HttpPost]
        public IActionResult AddProduct(Product p)
        {
            if (ModelState.IsValid)
            {
                p.ProductId = Product.GetNextId();
                Profile.AddProduct(p);
                return View("Index", Tuple.Create(Portfolio.CDs.Where(c => c.Months > MAX_MONTHS_CD_DISPLAY), Profile.Cart));
            }
            else
            {
                return View(p);
            }
        }

        [HttpGet] public IActionResult NewCD () => View();

        [HttpPost]
        public IActionResult NewCD(CD cd)
        {
            if (ModelState.IsValid)
            {
                Portfolio.AddCD(cd);
                return View("Index", Tuple.Create(Portfolio.CDs.Where(c => c.Months > MAX_MONTHS_CD_DISPLAY), Profile.Cart));
            }
            else
            {
                return View(cd);
            }
        }
    }
}
