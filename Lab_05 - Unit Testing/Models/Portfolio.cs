﻿using System.Collections.Generic;

namespace Lab_05___Unit_Testing.Models
{
    public class Portfolio
    {
        private static List<CD> cdList = new List<CD>();

        public static IEnumerable<CD> CDs
        {
            get
            {
                return cdList;
            }
        }

        public static void AddCD(CD cd)
        {
            cdList.Add(cd);
        }
    }
}
