﻿using System;

namespace Lab_05___Unit_Testing.Models
{
    [Serializable]
    public class Product : IEquatable<Product>
    {
        private static int idCount = -1;

        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }

        public bool Equals (Product other) => other.ProductId == this.ProductId;

        public static int GetNextId()
        {
            idCount++;
            return idCount;
        }
    }
}
