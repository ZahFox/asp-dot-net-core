﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lab_05___Unit_Testing.Models
{
    public class CD : IEquatable<CD>
    {
        public enum Banks { Altra_FCU = 1, Synchrony_Bank = 2, BMO_Harris = 3 };

        // Instance Member Variables //

        [Required(ErrorMessage = "Please choose a bank")]
        public Banks Bank { get; set; }

        [Required(ErrorMessage = "Please enter a term in months")]
        public uint? Months { get; set; }

        [Required(ErrorMessage = "Please enter a rate")]
        [Range(0.01, 100,
            ErrorMessage = "Rate must be between 0.01% and 100%")]
        public double? Rate { get; set; }

        [Required(ErrorMessage = "Please enter a deposit amount")]
        [DataType(DataType.Currency)]
        public decimal? DepositAmount { get; set; }

        [Required(ErrorMessage = "Please enter a purchase date")]
        [DataType(DataType.Date)]
        public DateTime? PurchaseDate { get; set; }

        public string getBankString()
        {
            switch (Bank)
            {
                case Banks.Altra_FCU:
                    return "Altra FCU";
                case Banks.Synchrony_Bank:
                    return "Synchrony Bank";
                case Banks.BMO_Harris:
                    return "BMO Harris";
                default:
                    throw new MissingFieldException("Your CD does not have a valid bank");
            }
        }

        public static string getBankString(Banks value)
        {
            switch (value)
            {
                case Banks.Altra_FCU:
                    return "Altra FCU";
                case Banks.Synchrony_Bank:
                    return "Synchrony Bank";
                case Banks.BMO_Harris:
                    return "BMO Harris";
                default:
                    throw new MissingFieldException("Your CD does not have a valid bank");
            }
        }


        // Constructors //

        public CD()
        {
            Banks[] banks = (Banks[])Enum.GetValues(typeof(Banks));
            Bank = banks[0];
            Months = 0;
            Rate = 0;
            DepositAmount = 0;
            PurchaseDate = new DateTime();
        }

        public CD (Banks bank, uint months, double rate, decimal depositAmount, DateTime purchaseDate)
        {
            Bank = bank;
            Months = months;
            Rate = rate;
            DepositAmount = depositAmount;
            PurchaseDate = purchaseDate;
        }


        // Business Logic Methods //

        [DataType(DataType.Date)]
        public DateTime getMaturityDate()
        {
            return PurchaseDate.Value.AddMonths( (int)Months );
        }

        public decimal getMaturityValue()
        {
            return (DepositAmount * (decimal)Math.Pow( (1 + ( (Rate.Value / 100) / 365) ),
                (365 * ( (getMaturityDate() - PurchaseDate).Value.TotalMilliseconds / 31536000000d) ) )).Value;
        }

        public bool Equals(CD other)
        {
            return (this.Bank == other.Bank && this.Months == other.Months &&
                this.Rate == other.Rate && this.DepositAmount == other.DepositAmount &&
                this.PurchaseDate == other.PurchaseDate);
        }
    }
}
