﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace TestIdentity.Models
{
    public class SeedData
    {
        public static void EnsurePopulatedAsync(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<AppIdentityDbContext>();
                context.Database.Migrate();

                if (!context.Roles.Any() || !context.Users.Any())
                {
                    const string DEFAULT_ADMIN_PASSWORD = "password";
                    var userManager = serviceScope.ServiceProvider.GetService<UserManager<AppUser>>();
                    var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();

                    // Get All Users and Roles (If there are any)
                    List<AppUser> users = userManager.Users.ToList();
                    List<IdentityRole> roles =  roleManager.Roles.ToList();

                    // Delte any Users or Roles that were found
                    for (int i = 0; i < users.Count; i++)
                    {
                        userManager.DeleteAsync(users[i]).GetAwaiter().GetResult();
                    }
                    for (int i = 0; i < roles.Count; i++)
                    {
                        roleManager.DeleteAsync(roles[i]).GetAwaiter().GetResult();
                    }

                    // Create the default data for the application
                    AppUser user = new AppUser
                    {
                        UserName = "admin",
                        Email = "admin@example.com"
                    };

                    List<string> roleNames = new List<string> { "Admin", "Service", "Application", "User" };

                    for (int i = 0; i < roleNames.Count; i++)
                    {
                        roleManager.CreateAsync(new IdentityRole(roleNames[i])).GetAwaiter().GetResult();
                    }

                    userManager.CreateAsync(user, DEFAULT_ADMIN_PASSWORD).GetAwaiter().GetResult();

                    context.SaveChanges();
                }
            }
        }
    }
}
