﻿using System.ComponentModel.DataAnnotations;

namespace TestIdentity.Models
{
    public class CreateRoleModel
    {
        [Required]
        public string Name { get; set; }
    }
}
