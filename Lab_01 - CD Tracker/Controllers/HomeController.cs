﻿using System;
using Microsoft.AspNetCore.Mvc;
using Lab_01___CD_Tracker.Models;


namespace Lab_01___CD_Tracker.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View(Portfolio.CDs);
        }

        [HttpGet]
        public IActionResult NewCD()
        {
            return View();
        }

        [HttpPost]
        public IActionResult NewCD(CD cd)
        {
            if (ModelState.IsValid)
            {
                Portfolio.AddCD(cd);
                return View("Index", Portfolio.CDs);
            }
            else
            {
                return View(cd);
            }
        }
    }
}
