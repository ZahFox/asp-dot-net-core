﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Lab_03___Razor_Essentials.Models
{
    public sealed class Inventory : IEnumerable<Product>
    {
        private const string HATURL = "https://static.vecteezy.com/system/resources/previews/000/132/128/non_2x/free-christmas-vector-hat-illustration.jpg";
        private const string SHOEURL = "https://static.vecteezy.com/system/resources/previews/000/073/222/non_2x/vector-sneaker.jpg";
        private const string SNOWBOARDURL = "https://static.vecteezy.com/system/resources/previews/000/083/712/non_2x/free-vector-snowboard-set.jpg";
        private const string JACKETURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Columbia_Sportswear_Jacket.jpg/391px-Columbia_Sportswear_Jacket.jpg";

        static private IEnumerable<Product> products = new List<Product>()
        {
            new Product{ ProductId = 1001, Name = "Hat", Description = "Keep Your Ears Warm", Price = 19.99M, Image = HATURL },
            new Product{ ProductId = 1002, Name = "Shoe", Description = "Shoes Speak Louder Than Words", Price = 49.99M, Image = SHOEURL },
            new Product{ ProductId = 1003, Name = "Snowboard", Description = "One Less Skier", Price = 159.99M, Image = SNOWBOARDURL },
            new Product{ ProductId = 1004, Name = "Jacket", Description = "Chill Without Being Cold", Price = 89.99M, Image = JACKETURL }
        };

        public static IEnumerable<Product> Products
        {
            get
            {
                return products;
            }
        }

        public IEnumerator<Product> GetEnumerator()
        {
            return Products.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static Product FindFirst(Func<Product, bool> selector)
        {
            foreach(Product p in products)
            {
                if (selector(p))
                {
                    return p;
                }
            }
            return null;
        }
    }
}
