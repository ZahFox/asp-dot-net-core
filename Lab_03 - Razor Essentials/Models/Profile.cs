﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lab_03___Razor_Essentials.Services;

namespace Lab_03___Razor_Essentials.Models
{
    public static class Profile
    {
        private static ProfileService service;

        private static List<Product> cart = new List<Product>();

        public static IEnumerable<Product> Cart { get => cart; }

        public static int CartCount => cart.Count;

        public static bool HasProfile { get; set; }

        public static string Username { get; set; }

        public static void SaveProfile()
        {
            if (service != null && Username != null)
            {
                HasProfile = true;

                service.WriteData(new ProfileData {
                    Username = Username,
                    Products = cart.ToArray() ?? new Product[] { }
                });
            }
        }

        private static void ConfigureProfile(ProfileData data)
        {
            HasProfile = true;
            Username = data.Username;
            for (int i = 0; i < data.Products.Length; ++i)
            {
                cart.Add(data.Products[i]);
            }
        }

        public static async void AddService(ProfileService newService) {
            System.Diagnostics.Debug.WriteLine("ALERT: \n\t->  Creating new profile service...");
            service = newService;
            ProfileData profileData = await service.ReadData();

            if (profileData == null)
            {
                System.Diagnostics.Debug.WriteLine("ALERT: \n\t->  No profile data found...");

                HasProfile = false;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("ALERT: \n\t->  Profile data was found!!!");

                ConfigureProfile(profileData);
            }
        }

        public static bool AddProduct(Product product)
        {
            if (!cart.Contains(product))
            {
                cart.Add(product);
                SaveProfile();
                return true;
            }
            return false;
        }

        public static bool DeleteProduct(int id)
        {
            if ( cart.Remove(cart.Find((p) => p.ProductId == id)) )
            {
                SaveProfile();
                return true;
            }
            return false;
        }

        public static bool ProductInCart (Product product) => cart.Contains(product);

    }


    [Serializable]
    public class ProfileData
    {
        public ProfileData() { }

        public Product[] Products { get; set; }

        [Required(ErrorMessage = "Please enter a valid Username")]
        [MinLength(8, ErrorMessage = "Please enter a Username that is atleast 8 characters long")]
        public string Username { get; set; }
    }
}
