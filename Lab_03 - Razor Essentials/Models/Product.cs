﻿using System;

namespace Lab_03___Razor_Essentials.Models
{
    [Serializable]
    public class Product : IEquatable<Product>
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }

        public bool Equals (Product other) => other.ProductId == this.ProductId;
    }
}
