﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab_03___Razor_Essentials.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lab_03___Razor_Essentials.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet] public ViewResult Index () => Profile.HasProfile ? View() : View("Signup");

        [HttpGet] public ViewResult Signup () => Profile.HasProfile ? View("Index") : View();

        [HttpPost]
        public IActionResult Signup(ProfileData data)
        {
            System.Diagnostics.Debug.WriteLine($"ALERT: username was {data.Username}");

            if (ModelState.IsValid)
            {
                Profile.Username = data.Username.Trim();
                Profile.SaveProfile();

                return View("Index");
            }
            else
            {
                return View("Signup", data);
            }
        }

        [HttpGet]
        public IActionResult Product(int id)
        {
            Product product = Inventory.FindFirst( p => p.ProductId == id );
            return Profile.HasProfile ? ( product != null ? View(product) : View("Index") ) : View("Signup");
        }

        [HttpPost]
        public IActionResult Product(Product product)
        {
            if (!Profile.HasProfile) return View("Signup");

            if (!Profile.AddProduct(product))
            {
                ViewBag.Error = $"The {product.Name} is already in your cart..";
            }

            return View(Inventory.FindFirst(p => p.ProductId == product.ProductId));
        }

        [HttpGet] public IActionResult Products () => Profile.HasProfile ?  View(Inventory.Products) : View("Signup");

        [HttpGet] public IActionResult Cart () => Profile.HasProfile ? View() : View("Signup");

        [HttpPost]
        public IActionResult Cart(int id)
        {
            Profile.DeleteProduct(id);

            return View();
        }
    }
}