﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Lab_03___Razor_Essentials.Models;

namespace Lab_03___Razor_Essentials.Services
{
    public sealed class ProfileService
    {
        private static ProfileService service;
        private const string FILE_PATH = "profile-data.dat";

        private ProfileService() { }

        public static ProfileService Service
        {
            get
            {
                if (service == null) service = new ProfileService();
                return service;
            }
        }

        public async Task<ProfileData> ReadData()
        {
            FileStream fs = null;
            byte[] rawData;

            try
            {
                using (fs = File.Open(FILE_PATH, FileMode.Open))
                {
                    // Read the raw data into a byte array
                    rawData = new byte[fs.Length];
                    await fs.ReadAsync(rawData, 0, (int)fs.Length);
 
                    // Deserialize the raw data into the ProfileData class
                    MemoryStream ms = new MemoryStream(rawData);
                    BinaryFormatter bf = new BinaryFormatter();
                    return (ProfileData)bf.Deserialize(ms);
                }  
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async void WriteData(ProfileData data)
        {
            FileStream fs = null;

            try
            {
                using (fs = File.Open(FILE_PATH, FileMode.Create))
                {
                    // Serialize the data into a byte array
                    MemoryStream ms = new MemoryStream();
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(ms, data);

                    // Write the raw data to drive
                    byte[] rawData = ms.ToArray();
                    await fs.WriteAsync(rawData, 0, rawData.Length);
                }
            }
            catch (Exception e)
            { }
        }
    }
}
