﻿namespace Lab_02___Language_Features.Models
{
    public class Product
    {
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public Product Related { get; set; }
        public string Category { get; set; } = "Watersports";
        public bool InStock { get; } = true;
        public bool NameBeginsWithS => Name?[0] == 'S';

        public bool NameBeginsWith => Name[0] == 'S';

        /*    ~~ PART 1 ~~
        private string category;
        public string Category {
            get {
                return category;
            }
            set {
                if (value == null) category = "";
                else category = value;
            }
        }
        */

        /// Constructor ///
        public Product(bool stock = true)
        {
            InStock = stock;
        }

        public static Product[] GetProducts()
        {
            Product kayak = new Product()
            {
                Name = "Kayak",
                Price = 275M
            };

            Product lifejacket = new Product(false)
            {
                Name = "Lifejacket",
                Price = 48.95M
            };

            kayak.Related = lifejacket;

            return new Product[] { kayak, lifejacket, null };
        }
    }
}
