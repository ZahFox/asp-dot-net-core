﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab_02___Language_Features.Models
{
    public static class MyExtensionMethods
    {
        /*
        public static decimal TotalPrices(this ShoppingCart cartParam)
        {
            decimal total = 0;
            foreach(Product sc in cartParam.Products)
            {
                total += sc?.Price ?? 0;
            }

            return total;
        }
        */

        public static decimal TotalPrices(this IEnumerable<Product> products)
        {
            decimal total = 0;
            foreach (Product sc in products)
            {
                total += sc?.Price ?? 0;
            }

            return total;
        }


        public static IEnumerable<Product> FilterByPrice(this IEnumerable<Product> products, decimal minPrice)
        {
            // List<Product> filteredProducts = new List<Product>();

            foreach(Product p in products)
            {
                if (p?.Price > minPrice)
                {
                    // filteredProducts.Add(p);
                    yield return p;
                }
            }

            // return filteredProducts;
        }


        public static IEnumerable<Product> FilterByName(this IEnumerable<Product> products, char firstChar)
        {
            foreach (Product p in products)
            {
                if (p?.Name?[0] == firstChar)
                {
                    yield return p;
                }
            }
        }


        // This extension method takes a delegate for an arugment
        public static IEnumerable<Product> Filter(this IEnumerable<Product> products, Func<Product, bool> selector)
        {
            foreach(Product p in products)
            {
                if (selector(p))
                {
                    yield return p;
                }
            }
        }
    }
}
