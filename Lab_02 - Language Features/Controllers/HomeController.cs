﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Lab_02___Language_Features.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Lab_02___Language_Features.Controllers
{
    public class HomeController : Controller
    {
        /*    ~~ PART 1 ~~ 
        public ViewResult Index()
        {
            return View( new string[] {"C#", "Language", "Features"} );
        }
        */


        //    ~~ PART 2 ~~
        //public ViewResult Index()
        //{
        //    List<string> results = new List<string>();

        //    foreach (Product p in Product.GetProducts())
        //    {
        //        string name = p?.Name ?? "<No Name>";
        //        decimal? price = p?.Price ?? 0;
        //        string related = p?.Related?.Name ?? "<No Relation>";

        //        /*    ~~ PART 1 ~~ 
        //        results.Add(string.Format("Name: {0}, Price: {1}, Related: {2}",
        //                    name, price, p?.Related?.Name));
        //        */

        //        results.Add($"Name: {name}, Price: {price:C2}, Related: {related}");
        //    }

        //    return View(results);
        //}


        //    ~~ PART 3 ~~ Using Initializer Contruction
        //public ViewResult Index()
        //{
        //    /*
        //    Dictionary<string, Product> products =
        //        new Dictionary<string, Product>()
        //        {
        //            { "Kayak", new Product() { Name = "Kayak", Price = 275M } },
        //            { "Lifejacket", new Product() { Name = "Lifejacket", Price = 48.95M } }
        //        };
        //    */

        //    Dictionary<string, Product> products =
        //        new Dictionary<string, Product>
        //        {
        //            ["Kayak"]      = new Product() { Name = "Kayak", Price = 275M },
        //            ["Lifejacket"] = new Product() { Name = "Lifejacket", Price = 48.95M }
        //        };

        //    // Product p = products["Kayak"];
        //    return View(products.Keys);
        //}


        //    ~~ PART 4 ~~ Using Extension Methods and Interfaces
        //public ViewResult Index()
        //{
        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProducts()
        //    };

        //    Product[] prodArr =
        //    {
        //        new Product{ Name = "Canoe", Price = 2000 },
        //        new Product{ Name = "Paddle", Price = 15}
        //    };

        //    decimal totalPrice = cart.TotalPrices();

        //    return View("Index", new string[] {
        //        $"Old Total: {totalPrice:C2}",
        //        $"New Total: {prodArr.TotalPrices():C2}"
        //    });

        //}


        //    ~~ PART 5 ~~ Using Lambda Expressions
        //public ViewResult Index()
        //{
        //    ShoppingCart cart = new ShoppingCart
        //    {
        //        Products = Product.GetProducts()
        //    };

        //    decimal expensiveTotal = cart.FilterByPrice(20).TotalPrices();
        //    decimal nameTotal = cart.FilterByName('K').TotalPrices();

        //    IEnumerable<Product> fProducts = cart.Filter(p => (p?.Price ?? 0) > 20);
        //    decimal priceTotal = fProducts.TotalPrices();

        //    decimal nameTotal2 = cart.Filter(p => p?.Name?[0] == 'K').TotalPrices();



        //    return View("Index", new string[] {
        //       $"Price Total: {expensiveTotal:C2}",
        //       $"Name Total: {nameTotal:C2}",
        //       $"Lambda Price Total: {priceTotal:C2}"
        //    });
        //}


        //    ~~ PART 6 ~~ Using Lambda Expressions pt. 2
        //public ViewResult Index()
        //{
        //    return View(Product.GetProducts().Select(p => p?.Name ?? "No Name"));
        //}

        //public ViewResult Index() => View(Product.GetProducts().Select(p => p?.Name ?? "No Name"));


        //    ~~ PART 7 ~~ Using Type Inference and Anonymous Types
        //public ViewResult Index()
        //{
        //    var names = new[] { "Kayak", "Lifejacket", "Soccer Ball" };
        //    return View(names);
        //}

        //public ViewResult Index()
        //{
        //    var products = new[]
        //    {
        //        new { Name = "Kayak",       Price = 275M },
        //        new { Name = "Lifejacket",  Price = 275M },
        //        new { Name = "Soccer Ball", Price = 19.50M },
        //        new { Name = "Corner Flag", Price = 34.95M }
        //    };

        //    // return View(products.Select(p => p.GetType().Name)); Get the anonymous data type
        //    return View(products.Select(p => p.Name));
        //}


        //    ~~ PART 8 ~~ Asynchronous Methods
        //public async Task<ViewResult> Index()
        //{
        //    long? length = await MyAsyncMethods.GetPageLength();
        //    return View(new string[] { $"Length: {length}" });
        //}


        //    ~~ PART 9 ~~ Getting Names
        public ViewResult Index()
        {
            var products = Product.GetProducts();

            return View(products.Select(p =>
                $"{nameof(p.Name)}: {p?.Name ?? "No Name"}, {nameof(p.Price)}: {p?.Price ?? 0}"));
        }
    }
}