﻿using Microsoft.AspNetCore.Mvc;
using Synomy.Models.ViewModels;
using Synomy.Services;
using System.Threading.Tasks;

namespace Synomy.Components
{
    public class Sidebar : ViewComponent
    {
        private IHttpClient notusClient;

        public Sidebar(IHttpClient notus)
        {
            notusClient = notus;
        }

        public async Task<IViewComponentResult> InvokeAsync() =>
            View( await NavMenuUtils.GetBrowseTopicNavMenu(notusClient) );
    }
}
