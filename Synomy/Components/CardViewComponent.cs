﻿using Synomy.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Synomy.Components
{
    public class CardViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(Card card)
        {
            return await Task.Run(() => {
                switch(card.CardType)
                {
                    case CardType.Default:
                        return View(card);
                    case CardType.Favorite:
                        return View("Favorite", card);
                    case CardType.CollectionFavorite:
                        return View("CollectionFavorite", card);
                    default:
                        return View(card);
                }
            });         
        }
    }
}
