﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Synomy.Models.Data;
using Synomy.Models.ViewModels;
using Synomy.Services;
using Synomy.Models.Api;
using Synomy.Infrastructure;
using System;

namespace Synomy.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IHttpClient notusClient;

        public NavigationMenuViewComponent(IHttpClient notus)
        {
            notusClient = notus;
        }

        public IViewComponentResult Invoke()
        {
            string controllerName = RouteData?.Values["controller"]?.ToString() ?? null;
            string actionName = RouteData?.Values["action"]?.ToString() ?? null;

            int id;
            Int32.TryParse(RouteData?.Values["id"]?.ToString() ?? null, out id);

            switch (controllerName)
            {
                case "Topics":
                    ViewBag.SelectedFilter = RouteData?.Values["filter"];
                    IEnumerable<Language> languages = notusClient.GetDataCollection<Language>("Language").GetAwaiter().GetResult();
                    IEnumerable<Topic> topics = notusClient.GetDataCollection<Topic>("Topic").GetAwaiter().GetResult();

                    return View(new List<FilterSection<string>>
                    {
                        new FilterSection<string>{
                            Title = "Filter By Language", Action = "Language", Controller = "Topics", Filters = languages
                            .Select(language => language.Name)
                            .Distinct()
                            .OrderBy(language => language)
                        },
                        new FilterSection<string>{
                            Title = "Filter By Topic Title", Action = "Title", Controller = "Topics", Filters = topics
                            .Select(topic => topic.Title)
                            .Distinct()
                            .OrderBy(topic => topic)
                        },
                    });
                case "Admin":
                    ViewBag.SelectedSection = RouteData?.Values["type"];
                    return View("CreateList", new List<FilterSection<string>>
                    {
                        new FilterSection<string>{
                            Title = "Add A New Language", Action = "Create", Controller = "Admin", WildCard = "Language"
                        },
                        new FilterSection<string>{
                            Title = "Add A New Topic", Action = "Create", Controller = "Admin", WildCard = "Topic"
                        },
                        new FilterSection<string>{
                            Title = "Add Topic To A Language", Action = "Create", Controller = "Admin", WildCard = "LanguageTopic"
                        },
                    });
                case "Favorite":
                    IEnumerable<FavoriteCollectionDTO> favoriteCollections = notusClient.GetDataCollection<FavoriteCollectionDTO>("FavoriteCollection").GetAwaiter().GetResult();

                    ViewBag.SelectedSection = RouteData?.Values["type"];
                    return View("Favorites", new List<FilterSection<NavLink>>
                    {
                        new FilterSection<NavLink>{
                            Title = "Create A New Collection", Action = "Create", Controller = "FavoriteCollection",
                        },
                        new FilterSection<NavLink>{
                            Title = "Current Collections", Action = "List", Controller = "FavoriteCollection", Filters = favoriteCollections
                            .Select(favCol => new NavLink { Id = favCol.FavoriteCollection.FavoriteCollectionId, Text = favCol.FavoriteCollection.Title })
                            .Distinct()
                            .OrderBy(favCol => favCol)
                        }
                    });
                case "FavoriteCollection":
                    if (actionName != "Create")
                    {
                        FavoriteCollectionDTO favoriteCol = notusClient.GetData<FavoriteCollectionDTO>("FavoriteCollection/1").GetAwaiter().GetResult();

                        return View("CollectionList", new List<FilterSection<string>>
                        {
                            new FilterSection<string>{
                                Title = "Edit Collection", Action = "Update", Controller = "FavoriteCollection", WildCardInt = id
                            },
                            new FilterSection<string>{
                                Title = "Delete Collection", Action = "Delete", Controller = "FavoriteCollection", WildCardInt = id
                            }
                        });
                    }
                    else
                    {
                        return View(new List<FilterSection<string>>());
                    }
                default:
                    return View(new List<FilterSection<string>>());
            }
        }
    }
}
