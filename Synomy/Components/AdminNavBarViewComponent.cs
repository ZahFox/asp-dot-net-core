﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Synomy.Components
{
    public class AdminNavBarViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.Run(() => {
                return View();
            });
        }
    }
}
