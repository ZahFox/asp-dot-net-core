﻿using Microsoft.AspNetCore.Mvc;

namespace Synomy.Components
{
    public class NavigationBarViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke() => View();
    }
}
