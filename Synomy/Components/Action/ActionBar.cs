﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Synomy.Components.Action
{
    public class ActionBar : ViewComponent
    {
        public string ViewToRender { get; set; } = "Default";

        public async Task<IViewComponentResult> InvokeAsync(IEnumerable<Action> actions)
        {
            foreach (Action action in actions) await action.LoadAction();
            return View(ViewToRender, actions);
        }
    }

    public static class ActionProvider
    {
        public static IEnumerable<Action> GetAdminActions()
        {
            return new List<Action>
            {
                new ManageContentAction{ },
                new ManageUsersAction{ },
                new ManageSettingsAction{ }
            };
        }
    }
}
