﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Synomy.Components.Action
{
    public class Action : ViewComponent
    {
        public virtual string Name { get; }

        public virtual async Task<IViewComponentResult> InvokeAsync(Action action) =>
            await Task.Run( () =>View(action.Name));

        public virtual async Task LoadAction() => await Task.Run(() => { });
    }

    public class ManageContentAction : Action
    {
        public override string Name { get; } = "ManageContent";

        public override async Task<IViewComponentResult> InvokeAsync(Action action) =>
            await Task.Run(() => View(action.Name));

        public override async Task LoadAction()
        {
            await Task.Run(() => { });
        }
    }

    public class ManageUsersAction : Action
    {
        public override string Name { get; } = "ManageUsers";

        public override async Task<IViewComponentResult> InvokeAsync(Action action) =>
            await Task.Run(() => View(action.Name));

        public override async Task LoadAction()
        {
            await Task.Run(() => { });
        }
    }

    public class ManageSettingsAction : Action
    {
        public override string Name { get; } = "ManageSettings";

        public override async Task<IViewComponentResult> InvokeAsync(Action action) =>
            await Task.Run(() => View(action.Name));

        public override async Task LoadAction()
        {
            await Task.Run(() => { });
        }
    }
}
