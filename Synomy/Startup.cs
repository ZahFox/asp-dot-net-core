﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Synomy.Services;
using Synomy.Infrastructure;
using System.IdentityModel.Tokens.Jwt;

namespace Synomy
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IHttpClient, NotusClient>();
            services.AddScoped<IFavorites, SessionFavorites>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ISessionScribe, SessionScribe>();

            services.AddMvc();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            })
                .AddCookie("Cookies")
                .AddOpenIdConnect("oidc", options =>
                {
                    options.SignInScheme = "Cookies";

                    options.Authority = Configuration["IdentityServer:Authority"];
                    options.RequireHttpsMetadata = false;

                    options.ClientId = Configuration["IdentityServer:ClientId"];
                    options.ClientSecret = Configuration["IdentityServer:ClientSecret"];
                    options.ResponseType = "code id_token";

                    options.SaveTokens = true;
                    options.GetClaimsFromUserInfoEndpoint = true;

                    options.Scope.Clear();
                    options.Scope.Add("openid");
                    options.Scope.Add("email");
                    options.Scope.Add("notus-role");
                    options.Scope.Add("notus.synomy");
                    options.Scope.Add("offline_access");

                    options.ClaimActions.MapUniqueJsonKey("notus-role", "notus-role");
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminOnly", builder =>
                {
                    builder.RequireClaim("notus-role", "Admin");
                });
            });

            services.AddMemoryCache();
            services.AddSession();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseSession();

            // All Custom Routing is Defined In The RouteMapper Class
            app.UseMvc(routes => {
                RouteMapper.GetRoutes(routes);
            });
        }
    }
}
