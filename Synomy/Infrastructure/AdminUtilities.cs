﻿using Microsoft.AspNetCore.Mvc;
using Synomy.Services;
using System.Threading.Tasks;

namespace Synomy.Infrastructure
{
    public class AdminUtilities
    {
        private readonly IHttpClient notus;

        public AdminUtilities(IHttpClient notus)
        {
            this.notus = notus;
        }

        public async Task<IActionResult> DeleteContent(string type, int id)
        {
            switch (type)
            {
                case "Language":
                    return new StatusCodeResult((int)(await notus.DeleteData(id, "Language")));
                case "LanguageTopic":
                    return new StatusCodeResult((int)(await notus.DeleteData(id, "LanguageTopic")));
                case "Topic":
                    return new StatusCodeResult((int)(await notus.DeleteData(id, "Topic")));
                case "Snippet":
                    return new StatusCodeResult((int)(await notus.DeleteData(id, "Topic")));
                default: return new BadRequestResult();
            }
        }
    }
}
