﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Synomy.Infrastructure
{
    public static class RouteMapper
    {
        public static void GetRoutes(IRouteBuilder routes)
        {
            // Custom Routing For the Topics Controller //
            routes.MapRoute(
                name: null,
                template: "{controller=Topics}/Title/{filter:regex(^All$)}/{topicPage=1}",
                defaults: new { action = "AllByTitle" }
            );

            routes.MapRoute(
                name: null,
                template: "{controller=Topics}/Title/{filter}/{topicPage=1}",
                defaults: new { action = "Title" }
            );

            routes.MapRoute(
                name: null,
                template: "{controller=Topics}/Language/{filter}/{topicPage=1}",
                defaults: new { action = "Language" }
            );

            routes.MapRoute(
                name: null,
                template: "{controller=Topics}/Toggle",
                defaults: new { action = "TitleToggle" }
            );

            // Default Routing //
            routes.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}"
            );

            routes.MapRoute(
                name: "Error",
                template: "Error",
                defaults: new { controller = "Error", action = "Error" }
            );

            // Catch All //
            routes.MapRoute(
               name: "catchall",
               template: "{*catchall}",
               defaults: new { controller = "Home", action = "Index" }
            );
        }
}
}
