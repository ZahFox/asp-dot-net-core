﻿namespace Synomy.Infrastructure
{
    public abstract class Filter
    {
        public abstract string GetFilter();
    }
}
