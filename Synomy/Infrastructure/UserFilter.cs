﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;

namespace Synomy.Infrastructure
{
    public class UserFilterAttribute : Attribute, IAuthorizationFilter
    {

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.User.Identity.IsAuthenticated &&
               !IsAnonymousMethod(context.Filters))
            {
                context.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                    { "controller", "Home" },
                    { "action", "Index" }
                });
            }
        }

        private static bool IsAnonymousMethod(IList<IFilterMetadata> filters)
        {
            foreach (var filter in filters)
            {
                if (filter.GetType() == typeof(AllowAnonymousFilter)) return true;
            }
            return false;
        }
    }
}
