﻿using System;

namespace Synomy.Infrastructure
{
    public class NavLink : IComparable<NavLink>
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int CompareTo(NavLink other)
        {
            return Id.CompareTo(other.Id);
        }
    }
}
