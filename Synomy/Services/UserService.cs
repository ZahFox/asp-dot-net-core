﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synomy.Services
{
    public class UserService : IUserService
    {
        private readonly HttpContext context;
        private readonly Dictionary<string, string> claims;

        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            context = httpContextAccessor.HttpContext;
            claims = context.User.Claims
                .ToDictionary(claim => claim.Type, claim => claim.Value);
        }

        public string GetEmail() => claims["email"];

        public string GetId() => claims["sub"];

        public UserRole GetUserRole() => ClaimToUserRole(claims["notus-role"]);

        public bool IsAdmin() => ClaimToUserRole(claims["notus-role"]) == UserRole.Admin;

        public bool IsAuthenticated() => context.User.Identity.IsAuthenticated;

        public bool IsModerator() => ClaimToUserRole(claims["notus-role"]) == UserRole.Moderator;

        public bool IsUser() => ClaimToUserRole(claims["notus-role"]) == UserRole.User;

        public async Task Logout()
        {
            await context.SignOutAsync("Cookies");
            await context.SignOutAsync("oidc");
        }

        private static UserRole ClaimToUserRole(string role)
        {
            switch (role)
            {
                case "Admin": return UserRole.Admin;
                case "Moderator": return UserRole.Moderator;
                case "User": return UserRole.User;
                default: throw new ArgumentException($"{role} is not a valid user role!");
            }
        }
    }
}
