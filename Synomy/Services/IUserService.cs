﻿using System.Threading.Tasks;

namespace Synomy.Services
{
    public enum UserRole { User, Moderator, Admin }

    public interface IUserService
    {
        bool IsAuthenticated();
        bool IsAdmin();
        bool IsUser();
        bool IsModerator();

        UserRole GetUserRole();
        string GetEmail();
        string GetId();

        Task Logout();
    }
}
