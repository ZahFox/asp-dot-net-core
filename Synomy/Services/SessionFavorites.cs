﻿
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Synomy.Infrastructure;
using Synomy.Models.Application;
using System.Linq;
using System.Threading.Tasks;
using Synomy.Models.Api;
using System.Net;

namespace Synomy.Services
{
    public class SessionFavorites : IFavorites
    {
        protected List<Favorite> favoriteCollection;

        private IHttpContextAccessor httpContextAccessor;
        private IHttpClient notusClient;
        private ISessionScribe sessionScribe;

        public SessionFavorites(IHttpContextAccessor httpContextAccessor, IHttpClient notus, ISessionScribe session)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.notusClient = notus;
            this.sessionScribe = session;

            favoriteCollection = httpContextAccessor.HttpContext.Session.GetJson<List<Favorite>>("Favorites");
            
            if (favoriteCollection == null)
            {
                favoriteCollection = sessionScribe.GetSessionFavorites() ?? new List<Favorite>();
                httpContextAccessor.HttpContext.Session.SetJson("Favorites", favoriteCollection);
            }
        }

        public List<Favorite> GetAllFavorites() => favoriteCollection;

        public async Task AddFavorite(FavoritePostDTO data)
        {
            var favorite = await notusClient.PostData<Favorite, FavoritePostDTO>(data, "Favorite");

            if (favorite != null)
            {
                favoriteCollection.Add(favorite);
                httpContextAccessor.HttpContext.Session.SetJson("Favorites", favoriteCollection);
            }
        }

        public async Task RemoveFavorite(int id)
        {
            HttpStatusCode code = await notusClient.DeleteData(id, "Favorite");

            if (code == HttpStatusCode.OK)
            {
                favoriteCollection.Remove(favoriteCollection.Find(f => f.FavoriteId == id));
                httpContextAccessor.HttpContext.Session.SetJson("Favorites", favoriteCollection);
            }
        }

        public async Task RemoveFavorite(string type, int id)
        {
            int favoriteId = favoriteCollection.Find(f => f.Content.ContentId == id &&
                f.Content.ContentType == type).FavoriteId;
            HttpStatusCode code = await notusClient.DeleteData(favoriteId, "Favorite");

            if (code == HttpStatusCode.OK)
            {
                favoriteCollection.Remove(favoriteCollection.Find(f => f.FavoriteId == favoriteId));
                httpContextAccessor.HttpContext.Session.SetJson("Favorites", favoriteCollection);
            }
        }

        public async Task Clear()
        {
            favoriteCollection.Clear();
            httpContextAccessor.HttpContext.Session.Remove("Favorites");
        }

        public bool InFavorites(int id, string type) =>
            favoriteCollection.Contains(
                favoriteCollection.Where(
                    favorite => favorite.Content.ContentId == id && favorite.Content.ContentType == type)
                .FirstOrDefault()
            );

    }
}
