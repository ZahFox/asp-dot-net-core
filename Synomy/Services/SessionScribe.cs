﻿using Newtonsoft.Json;
using Synomy.Models.Application;
using Synomy.Models.Data;
using System.Collections.Generic;
using System.Net.Http;

namespace Synomy.Services
{
    public class SessionScribe : ISessionScribe
    {
        public static string BaseUrl = "https://Okami.Services/Notus/";
        //public static string BaseUrl = "http://localhost:61118/";

        private List<Favorite> favorites;
        private List<ContentType> contentTypes;

        public SessionScribe()
        {
            HttpClient Client = new HttpClient();

            this.favorites = JsonConvert.DeserializeObject<List<Favorite>>(Client.GetStringAsync($"{BaseUrl}Favorite").GetAwaiter().GetResult());
            this.contentTypes = JsonConvert.DeserializeObject<List<ContentType>>(Client.GetStringAsync($"{BaseUrl}ContentType").GetAwaiter().GetResult());
        }

        public List<Favorite> GetSessionFavorites()
        {
            return favorites;
        }

        public List<ContentType> GetContentTypes()
        {
            return contentTypes;
        }
    }
}
