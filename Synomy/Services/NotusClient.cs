﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Synomy.Services
{
    public class NotusClient : IHttpClient, IDisposable
    {
        public static string BaseUrl = "https://Okami.Services/Notus/";
        private int requestHash;

        public static Dictionary<int, NotusClient> NotusClients = new Dictionary<int, NotusClient>();

        public HttpClient Client { get; }

        public NotusClient(IHttpContextAccessor httpContextAccessor, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                BaseUrl = "http://localhost:26000/";
            }

            requestHash = httpContextAccessor.HttpContext.GetHashCode();
            NotusClients.Add(requestHash, this);
            Client = new HttpClient();
        }

        public async Task<T> GetData<T>(string uri)
        {
            var res = await Client.GetStringAsync($"{BaseUrl}{uri}");
            return JsonConvert.DeserializeObject<T>(res);
        }

        public async Task<IEnumerable<T>> GetDataCollection<T>(string uri)
        {
            var res = await Client.GetStringAsync($"{BaseUrl}{uri}");
            return JsonConvert.DeserializeObject<List<T>>(res);
        }

        public async Task<ICollection<T>> GetDataICollection<T>(string uri)
        {
            var res = await Client.GetStringAsync($"{BaseUrl}{uri}");
            return JsonConvert.DeserializeObject<List<T>>(res);
        }

        public async Task<IQueryable<T>> GetDataQueryable<T>(string uri)
        {
            var res = await Client.GetStringAsync($"{BaseUrl}{uri}");
            return JsonConvert.DeserializeObject<IQueryable<T>>(res);
        }

        public async Task<T> PostData<T>(T data, string uri)
        {
            var res = await Client.PostAsync($"{BaseUrl}{uri}",
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            res.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await res.Content.ReadAsStringAsync());
        }

        public async Task<T> PostData<T, U>(U data, string uri)
        {
            var res = await Client.PostAsync($"{BaseUrl}{uri}",
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            res.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await res.Content.ReadAsStringAsync());
        }

        public async Task<T> PutData<T, U>(U data, string uri)
        {
            var res = await Client.PutAsync($"{BaseUrl}{uri}",
                new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            res.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await res.Content.ReadAsStringAsync());
        }

        public async Task<HttpStatusCode> DeleteData(int id, string uri)
        {
            var res = await Client.DeleteAsync($"{BaseUrl}{uri}/{id}");
            res.EnsureSuccessStatusCode();
            return res.StatusCode;
        }

        public void Dispose()
        {
            NotusClients.Remove(key: requestHash);
        }
    }
}
