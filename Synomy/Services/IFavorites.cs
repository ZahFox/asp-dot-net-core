﻿using Synomy.Models.Api;
using Synomy.Models.Application;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Synomy.Services
{
    public interface IFavorites
    {
        Task Clear();

        Task AddFavorite(FavoritePostDTO favorite);

        Task RemoveFavorite(int id);

        Task RemoveFavorite(string type, int id);

        List<Favorite> GetAllFavorites();

        bool InFavorites(int id, string type);
    }
}
