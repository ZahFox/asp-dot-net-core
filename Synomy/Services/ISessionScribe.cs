﻿using Synomy.Models.Application;
using Synomy.Models.Data;
using System.Collections.Generic;

namespace Synomy.Services
{
    public interface ISessionScribe
    {
        List<Favorite> GetSessionFavorites();

        List<ContentType> GetContentTypes();
    }
}
