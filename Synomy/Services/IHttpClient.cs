﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Synomy.Services
{
    public interface IHttpClient
    {
        HttpClient Client { get; }

        Task<T> GetData<T>(string uri); 

        Task<IEnumerable<T>> GetDataCollection<T>(string uri);

        Task<ICollection<T>> GetDataICollection<T>(string uri);

        Task<IQueryable<T>> GetDataQueryable<T>(string uri);

        Task<T> PostData<T>(T data, string uri);

        Task<T> PostData<T, U>(U data, string uri);

        Task<T> PutData<T, U>(U data, string uri);

        Task<HttpStatusCode> DeleteData(int id, string uri);
    }
}
