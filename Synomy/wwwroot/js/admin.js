﻿const DELETE_CONTENT_URI = 'Content/'
const DELETE = "deleted"; const CREATE = "created"; const UPDATE = "updated";
var adminStatus = null;

document.addEventListener('DOMContentLoaded', async (event) => {
    const colorInputs = document.querySelectorAll('input[color-input]')
    const urlInputs = document.querySelectorAll('input[url-input]')
    adminStatus = document.querySelector('[admin-status]')

    if (colorInputs) await handleColorInputs(colorInputs)
    if (urlInputs) await handleUrlInputs(urlInputs)

    await handleManageContentPage()
})

async function handleColorInputs(inputs) {
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].setAttribute('type', 'color')
        inputs[i].classList.add('color-input')
    }
}

async function handleUrlInputs(inputs) {
    for (let i = 0; i < inputs.length; i++) {
        if (!inputs[i].value) inputs[i].value = `https://`
        inputs[i].setAttribute('type', 'url')
        inputs[i].classList.add('url-input')
    }
}

async function handleManageContentPage() {
    let contentHeading = document.querySelector('[manage-content]')

    if (contentHeading) {
        let editButtons = document.querySelectorAll('[edit-button]')
        let deleteButtons = document.querySelectorAll('[delete-button]')

        if (editButtons && deleteButtons) {
            for (let i = 0; i < editButtons.length; i++) {
                editButtons[i].addEventListener('click', async () => {
                    await editContent({
                        type: editButtons[i].getAttribute('content-type'),
                        id: editButtons[i].getAttribute('content-id')
                    })
                })
            }

            for (let i = 0; i < deleteButtons.length; i++) {
                deleteButtons[i].addEventListener('click', async () => {
                    await deleteContent({
                        type: deleteButtons[i].getAttribute('content-type'),
                        id: deleteButtons[i].getAttribute('content-id')
                    })
                })
            }
        }
    }
}

async function deleteContent({ type, id }) {
    await axios.delete(`${DELETE_CONTENT_URI}${type}/${id}`)
        .then(async res => {
            if (res.status === 200) {
                await handleSuccess({ action: DELETE, type, id })
            }
            else {
                await handleFailure({ action: DELETE, type, id })
            }
        })
}

async function editContent({ type, id }) {
    window.location.href = `Update/${type}/${id}`
}

async function handleSuccess({ action, type, id }) {
    let itemToRemove = document.querySelector(`button[content-type="${type}"][content-id="${id}"]`)
        .parentElement.parentElement.parentElement
    itemToRemove.parentElement.removeChild(itemToRemove)

    if (adminStatus) {
        $().alert('close')

        adminStatus.innerHTML = 
        `
        <div class="alert fade show alert-primary" data-dismiss="alert" role="alert">
            The ${type} was successfully ${action}
        </div>
        `
        $('.alert').alert()

        adminStatus.addEventListener('click', async () => $().alert('close'))
    }
}

async function handleFailure({ action, type, id }) {
    if (adminStatus) {
        $().alert('close')

        adminStatus.innerHTML =
            `
        <div class="alert fade show alert-danger" data-dismiss="alert" role="alert">
            The ${type} failed to be ${action}
        </div>
        `
        $('.alert').alert()

        adminStatus.addEventListener('click', async () => $().alert('close'))
    }
}