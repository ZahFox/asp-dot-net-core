﻿var filterToggles = []
var filterToggleButtons = []
var glowingElements = []

document.addEventListener('DOMContentLoaded', (event) => {
    const curtain = document.querySelector('[curtain]')
    const favoriteListAddButtons = document.querySelectorAll('button[action="add-favorite-to-collection"]')
    const languageListDeleteButtons = document.querySelectorAll('button[action="delete"]')
    const languageListEditButtons = document.querySelectorAll('button[action="edit"]')
    const navBarChildren = document.querySelectorAll('nav > *')
    const navBar = document.querySelector('.navbar')
    const navMenu = document.querySelector('.nav-menu')
    const renderBody = document.querySelector('.render-body')
    const sidebar = document.getElementById('sidebar')
    const sidebarCollapse = document.getElementById('sidebarCollapse')

    if (findAndBindFilterToggles()) {
        loadToggleData()
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    // from: window-resize-handlers.js
    if (curtain && navMenu && renderBody && sidebar) {
        bindWindowResizeHandlers({ curtain, navMenu, renderBody, sidebar })
    }

    // from: sidebar.js
    if (curtain && sidebar && sidebarCollapse) {
        bindSidebar({ curtain, sidebar, sidebarCollapse })
    }

    // from: language-list.js
    if (languageListEditButtons.length > 0 && languageListDeleteButtons.length > 0) {
        bindLanguageListActionHandlers({ languageListEditButtons, languageListDeleteButtons })
    }

    // from: favorite-list.js
    if (favoriteListAddButtons.length > 0) {
        bindFavoriteListActionHandlers({ favoriteListAddButtons })
    }

    bindGlowToDOM()
})

function bindGlowToDOM() {
    const appTitle = document.querySelector('.app-title')
    const appAnchor = document.querySelector('.app-title > a')

    if (appTitle && appAnchor) {
        appAnchor.addEventListener('mouseenter', () => {
            glowingElements['app-title'] = true
            setTimeout(() => { if (glowingElements['app-title'] === true) enablePurpleGlow(appTitle) }, 100)
        })
        appAnchor.addEventListener('mouseleave', () => {
            glowingElements['app-title'] = false
            setTimeout(() => { if (glowingElements['app-title'] === false) disablePurpleGlow(appTitle) }, 1000)
        })
    }

    const filterSectionHeaders = document.querySelectorAll('.filter-section-header')

    if (filterSectionHeaders) {
        for (let i = 0; i < filterSectionHeaders.length; i++) {
            filterSectionHeaders[i].addEventListener('mouseenter', () => {
                glowingElements[`filter-section-${i}`] = true
                setTimeout(() => { if (glowingElements[`filter-section-${i}`] === true) enablePurpleGlow(filterSectionHeaders[i]) }, 100)
            })
            filterSectionHeaders[i].addEventListener('mouseleave', () => {
                glowingElements[`filter-section-${i}`] = false
                setTimeout(() => { if (glowingElements[`filter-section-${i}`] === false) disablePurpleGlow(filterSectionHeaders[i]) }, 1000)
            })
        }
    }
}

function enablePurpleGlow(element) { element.classList.add('purple-glow') }

function disablePurpleGlow(element) { element.classList.remove('purple-glow') }

function findAndBindFilterToggles() {
    filterToggleSearch = document.querySelectorAll('button[data-toggle="collapse"]')

    if (filterToggleSearch.length > 0) {
        for (let i = 0; i < filterToggleSearch.length; i++) {

            if (filterToggleSearch[i].firstElementChild) {
                let filterId = filterToggleSearch[i].firstElementChild.id
                filterToggles[filterId] = ''
                filterToggleButtons[filterId] = filterToggleSearch[i].nextElementSibling

                filterToggleSearch[i].addEventListener('click', function (e) {
                    let filterStyles = filterToggleSearch[i].nextElementSibling.classList
                    let filterCSS = ''

                    for (let i = 0; i < filterStyles.length; i++) filterCSS += `${filterStyles[i]} `
                    filterCSS = filterCSS.trim();

                    if (filterCSS === 'collapse') filterCSS = 'collapse show'
                    else filterCSS = 'collapse'

                    filterToggles[filterId] = filterCSS

                    saveToggleData(filterId)
                })
            }
        }

        return true
    }

    return false
}

function loadToggleData() {
    for (let key in filterToggles) {
        try { filterToggles[key] = localStorage.getItem(key) }
        catch (e) {
            console.log(e)
        }   
    }

    for (let key in filterToggles) {
        if (filterToggles[key] && filterToggles[key] !== '') {
            filterToggleButtons[key].className = filterToggles[key]
        }
    }
}

function saveToggleData(id) {
    for (let key in filterToggles) {
        if (key === id) {
            try { localStorage.setItem(key, filterToggles[key]) }
            catch (e) {
                console.log(e)
            }
        }
        else {
            localStorage.removeItem(key)
        }
        
    }
}