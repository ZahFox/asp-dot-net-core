﻿const minWindowSizeForNavMenu = 1200

function bindWindowResizeHandlers({ curtain, navMenu, renderBody, sidebar }) {
    window.addEventListener('resize', async () => await $("body").css("padding-top", $(".fixed-top").height() + 30))

    if (navMenu && renderBody) {
        window.addEventListener('resize', async () => {
            let currentWidth = window.innerWidth

            if (currentWidth >= minWindowSizeForNavMenu) {
                sidebar.classList.remove('active')
                curtain.classList.remove('hidden')
                curtain.classList.remove('visible')
            }

            if (currentWidth < minWindowSizeForNavMenu) await stretchRenderBody(renderBody)
            else await shrinkRenderBody(renderBody)
        })
    }

    $(function () {
        $("body").css("padding-top", $(".fixed-top").height() + 30)
    });
}

async function stretchRenderBody(renderBody) {
    renderBody.classList.remove('col-lg-9')
    renderBody.classList.add('col-lg-12')
}

async function shrinkRenderBody(renderBody) {
    renderBody.classList.add('col-lg-9')
    renderBody.classList.remove('col-lg-12')
}
