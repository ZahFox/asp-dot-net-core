﻿function bindSidebar({ curtain, sidebar, sidebarCollapse }) {
    curtain.addEventListener('click', async () => {
        if (curtain.classList.contains('visible')) {
            await toggleSidebar({ curtain, sidebar })
        }
    })

    sidebarCollapse.addEventListener('click', async () => {
        await toggleSidebar({ curtain, sidebar })
    })
}

async function toggleSidebar({ curtain, sidebar }) {
    if (sidebar.classList.contains('active')) sidebar.classList.remove('active')
    else sidebar.classList.add('active')

    if (curtain.classList.contains('visible')) {
        curtain.classList.remove('visible')
        curtain.classList.add('hidden')
    }
    else {
        curtain.classList.remove('hidden')
        curtain.classList.add('visible')
    }
}
