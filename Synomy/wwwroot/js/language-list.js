﻿function bindLanguageListActionHandlers({ languageListEditButtons, languageListDeleteButtons }) {
    for (let i = 0; i < languageListDeleteButtons.length; i++) {
        let languageId = languageListDeleteButtons[i].getAttribute('language')
        languageListDeleteButtons[i].addEventListener('click', async () => await showDeleteModal(languageId))
    }
}

async function showDeleteModal(languageId) {
    await $('#deleteModal').modal({
        show: true
    })
}