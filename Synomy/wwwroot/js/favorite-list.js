﻿const GET_EXCLUSIVE_COLLECTIONS_URL = 'https://Okami.Services/Notus/FavoriteCollection/Excluding/'
const ADD_FAVORITE_TO_COLLECTION_URL = 'https://Okami.Services/Notus/FavoriteCollection/'

function bindFavoriteListActionHandlers({ favoriteListAddButtons }) {
    document.querySelector('#submit-add-to-collection').addEventListener('click', async () => {

        await submitFavoriteToCollection({
            collectionId: await getCollectionId(),
            favoriteId: await getFavoriteId(),
            fail: await getFavoriteId(),
            success: async (data) =>
            {
                document.querySelector('#cancel-add-to-collection').click()
            }, fail: async (error) => console.log(error)
        })
    })

    for (let i = 0; i < favoriteListAddButtons.length; i++) {
        let favoriteId = favoriteListAddButtons[i].getAttribute('favorite-id')
        favoriteListAddButtons[i].addEventListener('click', async () => await showAddToCollectionModal(favoriteId))
    }

}

async function showAddToCollectionModal(favoriteId) {

    let availableCollections;

    await getAvailableCollections({
        id: favoriteId,
        success: (data) => {
            availableCollections = data
        },
        fail: (error) => { console.log(error) }
    })

    document.querySelector('.collection-select').innerHTML =
        `
        <select favorite-id=${favoriteId} collection-select>
          ${
            availableCollections.map(col =>
            `<option value="${col.favoriteCollectionId}">${col.title}</option>`)
          }       
        </select>
        `;

    await $('#addToCollectionModal').modal({
        show: true
    })
}

async function getCollectionData() {

}

async function getCollectionId() {
    return document.querySelector('[collection-select]').value
}

async function getFavoriteId() {
    return document.querySelector('[collection-select]').getAttribute('favorite-id')
}

async function getAvailableCollections({ id, success, fail }) {
    await axios.get(`${GET_EXCLUSIVE_COLLECTIONS_URL}${id}`)
    .then(function (response) {
        success(response.data);
    })
    .catch(function (error) {
        fail(error)
    });
}

async function submitFavoriteToCollection({ collectionId, favoriteId, success, fail }) {
    await axios.post(`${ADD_FAVORITE_TO_COLLECTION_URL}${collectionId}/${favoriteId}`, {})
    .then(function (response) {
        success(response.data);
    })
    .catch(function (error) {
        fail(error)
    })
}