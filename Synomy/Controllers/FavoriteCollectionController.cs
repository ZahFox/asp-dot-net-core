﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Synomy.Infrastructure;
using Synomy.Models.Api;
using Synomy.Models.Application;
using Synomy.Models.ViewModels;
using Synomy.Services;

namespace Synomy.Controllers
{
    [UserFilter]
    public class FavoriteCollectionController : Controller
    {
        private int PageSize = 5;

        private IHttpClient notusClient;
        private IFavorites favorites;
        private ISessionScribe sessionScribe;

        public FavoriteCollectionController(IHttpClient notus, IFavorites fav, ISessionScribe session)
        {
            notusClient = notus;
            favorites = fav;
            sessionScribe = session;
        }

        public async Task<IActionResult> List(int id)
        {
            var favCol = await notusClient.GetData<FavoriteCollectionDTO>($"FavoriteCollection/{id}");

            return View(new FavoriteCollectionViewModel {
                FavoriteCollection = favCol.FavoriteCollection,
                CardList = CreateFavoriteCardList(favCol.FavoriteCollection.FavoriteCollectionId, favCol.Favorites)
            });
        }

        [HttpGet]
        public IActionResult Create() => View();

        public async Task<IActionResult> Update(int id)
        {
            var col = await notusClient.GetData<FavoriteCollectionDTO>($"FavoriteCollection/{id}");
            return View(col.FavoriteCollection);
        }

        [HttpPost]
        public async Task<IActionResult> Update(Models.Data.FavoriteCollection collection)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PutData<FavoriteCollectionDTO, Models.Data.FavoriteCollection>(collection, "FavoriteCollection");
                return View("List", new FavoriteCollectionViewModel
                {
                    FavoriteCollection = res.FavoriteCollection,
                    CardList = CreateFavoriteCardList(res.FavoriteCollection.FavoriteCollectionId, res.Favorites)
                });
            }
            else
            {
                return View("Create", collection);
            }
        }


        public async Task<RedirectToActionResult> Delete(int id)
        {
            await notusClient.DeleteData(id, $"FavoriteCollection");
            return RedirectToAction(controllerName: "Favorite", actionName: "List");
        }

        [HttpPost]
        public async Task<IActionResult> RemoveFromCollection(int collectionId, int favoriteId)
        {
            await notusClient.DeleteData(favoriteId, $"FavoriteCollection/{collectionId}/Favorite");

            var favCol = await notusClient.GetData<FavoriteCollectionDTO>($"FavoriteCollection/{collectionId}");

            return View("List", new FavoriteCollectionViewModel
            {
                FavoriteCollection = favCol.FavoriteCollection,
                CardList = CreateFavoriteCardList(favCol.FavoriteCollection.FavoriteCollectionId, favCol.Favorites)
            });
        }

        [HttpPost]
        public async Task<IActionResult> Create(Models.Application.FavoriteCollection collection)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PostData<FavoriteCollectionDTO, Models.Application.FavoriteCollection>(collection, "FavoriteCollection");
                return View("List", new FavoriteCollectionViewModel
                {
                    FavoriteCollection = res.FavoriteCollection,
                    CardList = CreateFavoriteCardList(res.FavoriteCollection.FavoriteCollectionId, res.Favorites)
                });
            }
            else
            {
                return View("Create", collection);
            }
        }

        private CardListViewModel<Card> CreateFavoriteCardList(int id, ICollection<Favorite> data, string filterType = null, string filter = null, int topicPage = 1) =>
            new CardListViewModel<Card>
            {
                CardType = CardType.CollectionFavorite,
                Data = data.Skip((topicPage - 1) * PageSize).Take(PageSize)
                .Select(item => new Card
                {
                    CardType = CardType.CollectionFavorite,
                    Content = item.Content,
                    IsFavorited = true,
                    OptionalString1 = item.Content.ContentType,
                    OptionalInt1 = id,
                    OptionalInt2 = item.FavoriteId
                }),
                Controller = RouteData.Values["controller"].ToString(),
                FilterType = filterType,
                FilterValue = filter,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = topicPage,
                    ItemsPerPage = PageSize,
                    TotalItems = data.Count
                }
            };
    }
}