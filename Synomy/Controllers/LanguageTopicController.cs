﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Synomy.Models.Api;
using Synomy.Models.Data;
using Synomy.Models.ViewModels;
using Synomy.Services;

namespace Synomy.Controllers
{
    public class LanguageTopicController : Controller
    {
        private IHttpClient notusClient;

        public LanguageTopicController(IHttpClient notusClient)
        {
            this.notusClient = notusClient;
        }

        public async Task<IActionResult> Index(int id) =>
            View(new LanguageTopicDetails
            {
                LanguageTopic = await notusClient.GetData<LanguageTopicDTO>($"LanguageTopic/{id}"),
                Snippets = await notusClient.GetDataICollection<Snippet>($"Snippet/Topic/{id}")
            });
    }
}