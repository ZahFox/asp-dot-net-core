﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Synomy.Infrastructure;
using Synomy.Models.Api;
using Synomy.Models.Application;
using Synomy.Models.Data;
using Synomy.Models.ViewModels;
using Synomy.Services;
using System.Linq;
using System.Threading.Tasks;

namespace Synomy.Controllers
{
    [Authorize(Policy = "AdminOnly")]
    [Route("Admin")]
    public class AdminController : Controller
    {
        private IHttpClient notusClient;

        public AdminController(IHttpClient notus)
        {
            notusClient = notus;
        }

        public IActionResult Index() => View();

        [HttpGet("Create/{type}")]
        public IActionResult Create(string type)
        {
            switch (type)
            {
                case "Language":
                    return View("LanguageForm", new AdminLanguageForm
                    {
                        Method = "POST",
                        Language = null
                    });
                case "LanguageTopic":
                    return View("LanguageTopicForm", new AdminLanguageTopicForm
                    {
                        Method = "POST", LanguageTopic = null
                    });
                case "Topic":
                    return View("TopicForm", new AdminTopicForm
                    {
                        Method = "POST",
                        Topic = null
                    });
                default:
                    return RedirectToAction("Index");
            }
        }

        [HttpGet("Update/{type}/{id:int}")]
        public async Task<IActionResult> Update(string type, int id)
        {
            switch (type)
            {
                case "Language":
                    var language = await notusClient.GetData<Language>($"Language/{id}");
                    if (language != null)
                    {
                        return View("LanguageForm", new AdminLanguageForm
                        {
                            Method = "PUT",
                            Language = language
                        });
                    }
                    TempData["Status"] = "Error"; TempData["Message"] = $"There is no language with an ID of {id}";
                    return RedirectToAction("ManageContent");
                case "LanguageTopic":
                    var languageTopic = await notusClient.GetData<LanguageTopic>($"LanguageTopic/{id}");
                    if (languageTopic != null)
                    {
                        return View("LanguageTopicForm", new AdminLanguageTopicForm
                        {
                            Method = "PUT",
                            LanguageTopic = languageTopic
                        });
                    }
                    TempData["Status"] = "Error"; TempData["Message"] = $"There is no language topic with an ID of {id}";
                    return RedirectToAction("ManageContent");
                case "Topic":
                    var topic = await notusClient.GetData<Topic>($"Topic/{id}");
                    if (topic != null)
                    {
                        return View("TopicForm", new AdminTopicForm
                        {
                            Method = "PUT",
                            Topic = topic
                        });
                    }
                    TempData["Status"] = "Error"; TempData["Message"] = $"There is no topic with an ID of {id}";
                    return RedirectToAction("ManageContent");
                default:
                    return RedirectToAction("ManageContent");
            }
        }

        [HttpPost("Create/{type:regex(^Topic$)}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Topic topic)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PostData(topic, "Topic");
                TempData["Status"] = "Success"; TempData["Message"] = $"The new Topic was successfully created";
                return RedirectToAction("ManageContent");
            }
            else
            {
                return View("TopicForm", new AdminTopicForm
                {
                    Method = "POST",
                    Topic = topic
                });
            }
        }

        [HttpPost("Update/{type:regex(^Topic$)}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(Topic topic)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PutData<Topic, Topic>(topic, "Topic");
                TempData["Status"] = "Success"; TempData["Message"] = $"The existing Topic was successfully updated";
                return RedirectToAction("ManageContent");
            }
            else
            {
                return View("TopicForm", new AdminTopicForm
                {
                    Method = "PUT",
                    Topic = topic
                });
            }
        }

        [HttpPost("Create/{type:regex(^Language$)}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Language language)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PostData(language, "Language");
                TempData["Status"] = "Success"; TempData["Message"] = $"The new Language was successfully created";
                return RedirectToAction("ManageContent");
            }
            else
            {
                return View("LanguageForm", new AdminLanguageForm
                {
                    Method = "POST",
                    Language = language
                });
            }
        }

        [HttpPost("Update/{type:regex(^Language$)}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(Language language)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PutData<Language, Language>(language, "Language");
                TempData["Status"] = "Success"; TempData["Message"] = $"The existing Language was successfully updated";
                return RedirectToAction("ManageContent");
            }
            else
            {
                return View("LanguageForm", new AdminLanguageForm
                {
                    Method = "PUT",
                    Language = language
                });
            }
        }

        [HttpPost("Create/{type:regex(^LanguageTopic$)}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LanguageTopic languageTopic)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PostData(languageTopic, "LanguageTopic");
                TempData["Status"] = "Success"; TempData["Message"] = $"The new LanguageTopic was successfully created";
                return RedirectToAction("ManageContent");
            }
            else
            {
                return View("LanguageTopicForm", new AdminLanguageTopicForm
                {
                    Method = "POST",
                    LanguageTopic = languageTopic
                });
            }
        }

        [HttpPost("Update/{type:regex(^LanguageTopic$)}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(LanguageTopic languageTopic)
        {
            if (ModelState.IsValid)
            {
                var res = await notusClient.PutData<LanguageTopicDTO, LanguageTopic>(languageTopic, "LanguageTopic");
                TempData["Status"] = "Success"; TempData["Message"] = $"The existing Language was successfully updated";
                return RedirectToAction("ManageContent");
            }
            else
            {
                return View("LanguageTopicForm", new AdminLanguageTopicForm
                {
                    Method = "PUT",
                    LanguageTopic = languageTopic
                });
            }
        }

        [HttpDelete("Content/{type}/{id}")]
        public async Task<IActionResult> Delete(string type, int id) =>
            await new AdminUtilities(notusClient).DeleteContent(type, id);

        [HttpGet("ManageContent")]
        public async Task<IActionResult> ManageContent()
        {
            return View(new AdminContentList
            {
                Languages = (await notusClient.GetDataICollection<Language>("Language"))
                    .Select(language => ContentUtilities.CreateContent(language)),
                LanguageTopics = (await notusClient.GetDataICollection<LanguageTopicDTO>("LanguageTopic"))
                    .Select(languageTopic => ContentUtilities.CreateContent(languageTopic)),
                Topics = (await notusClient.GetDataICollection<Topic>("Topic"))
                    .Select(topic => ContentUtilities.CreateContent(topic)),
                Snippets = (await notusClient.GetDataICollection<Snippet>("Snippet"))
                    .Select(snippet => ContentUtilities.CreateContent(snippet))
            });
        }
    }
}