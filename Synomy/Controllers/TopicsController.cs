﻿using Microsoft.AspNetCore.Mvc;
using Synomy.Models.ViewModels;
using System.Linq;
using System.Collections.Generic;
using Synomy.Models.Api;
using System.Threading.Tasks;
using Synomy.Services;
using System.Web;
using Synomy.Models.Application;

namespace Synomy.Controllers
{
    public class TopicsController : Controller
    {
        private IHttpClient notusClient;
        private IFavorites favorites;

        public TopicsController(IHttpClient notusClient, IFavorites favorites)
        {
            this.notusClient = notusClient;
            this.favorites = favorites;
        }

        public int PageSize = 5;

        public async Task<IActionResult> Index() =>
            View("List", CreateLanguageTopicCardList(await notusClient.GetDataICollection<LanguageTopicDTO>("LanguageTopic"), "Title", "All"));

        public async Task<IActionResult> AllByTitle(int topicPage = 1) =>
            View("List", CreateLanguageTopicCardList(await notusClient.GetDataICollection<LanguageTopicDTO>($"LanguageTopic/Topic"), "Title", "All", topicPage));

        public async Task<IActionResult> Title(string filter, int topicPage = 1) =>
            View("List", CreateLanguageTopicCardList(await notusClient.GetDataICollection<LanguageTopicDTO>($"LanguageTopic/Topic/{filter}"), "Title", filter, topicPage));

        public async Task<IActionResult> Language(string filter, int topicPage = 1) =>
            View("List", CreateLanguageTopicCardList(await notusClient.GetDataICollection<LanguageTopicDTO>($"LanguageTopic/Language/{HttpUtility.UrlEncode(filter)}"), "Language", filter, topicPage));

        public async Task<IActionResult> TitleToggle() =>
            View("List", CreateLanguageTopicCardList(await notusClient.GetDataICollection<LanguageTopicDTO>("LanguageTopic"), "Title", "All"));

        private CardListViewModel<Card> CreateLanguageTopicCardList(ICollection<LanguageTopicDTO> data, string filterType = null, string filter = null, int topicPage = 1) =>
            new CardListViewModel<Card>
            {
                CardType = CardType.Default,
                Data = data.Skip((topicPage - 1) * PageSize).Take(PageSize)
                .Select(item => new Card
                {
                    CardType = CardType.Default,
                    Content = ContentUtilities.CreateContent(item),
                    IsFavorited = favorites.InFavorites(item.LanguageTopicId, "LanguageTopic")
                }),
                Controller = RouteData.Values["controller"].ToString(),
                FilterType = filterType,
                FilterValue = filter,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = topicPage,
                    ItemsPerPage = PageSize,
                    TotalItems = data.Count
                }
            };
    }
}