﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Synomy.Infrastructure;
using Synomy.Models.Api;
using Synomy.Models.Application;
using Synomy.Models.ViewModels;
using Synomy.Services;

namespace Synomy.Controllers
{
    [UserFilter]
    public class FavoriteController : Controller
    {
        private int PageSize = 5;

        private IHttpClient notusClient;
        private IFavorites favorites;
        private ISessionScribe sessionScribe;

        public FavoriteController(IHttpClient notus, IFavorites fav, ISessionScribe session)
        {
            notusClient = notus;
            favorites = fav;
            sessionScribe = session;
        }

        [HttpPost]
        public async Task<IActionResult> AddToFavorites(string type, int id, string user)
        {
            if (type != null && id > -1)
            {
                FavoritePostDTO favorite = new FavoritePostDTO
                {
                    ContentId = id,
                    ContentTypeId = sessionScribe.GetContentTypes().Find(ct => ct.Title == type).ContentTypeId,
                    UserId = user
                };

                await favorites.AddFavorite(favorite);
            }

            var favoriteList = favorites.GetAllFavorites();
            return View("List", CreateFavoriteCardList(favoriteList));
        }

        public async Task<IActionResult> RemoveFromFavorites(string type, int id, string user)
        {
            await favorites.RemoveFavorite(type, id);
            return View("List", CreateFavoriteCardList(favorites.GetAllFavorites()));
        }

        public IActionResult List()
        {
            var favoriteList = favorites.GetAllFavorites();
            return View(CreateFavoriteCardList(favoriteList));
        }

        private CardListViewModel<Card> CreateFavoriteCardList(ICollection<Favorite> data, string filterType = null, string filter = null, int topicPage = 1) =>
            new CardListViewModel<Card>
            {
                CardType = CardType.Favorite,
                Data = data.Skip((topicPage - 1) * PageSize).Take(PageSize)
                .Select(item => new Card
                {
                    CardType = CardType.Favorite,
                    Content = item.Content,
                    IsFavorited = true,
                    OptionalString1 = item.Content.ContentType,
                    OptionalInt1 = item.FavoriteId
                }),
                Controller = RouteData.Values["controller"].ToString(),
                FilterType = filterType,
                FilterValue = filter,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = topicPage,
                    ItemsPerPage = PageSize,
                    TotalItems = data.Count
                }
            };
    }
}