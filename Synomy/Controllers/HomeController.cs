﻿using Microsoft.AspNetCore.Mvc;

namespace Synomy.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return View();

            }
            else
            {
                return RedirectToRoute(new
                {
                    controller = "User",
                    action = "Index"
                });
            }
        }  
    }
}
