﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Synomy.Infrastructure;
using Synomy.Services;
using System.Threading.Tasks;

namespace Synomy.Controllers
{
    [UserFilter]
    public class UserController : Controller
    {
        private IUserService userService { get; set; }

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [Authorize]
        public async Task<IActionResult> Index() =>
            await Task.Run(() => View(userService));

        [AllowAnonymous]
        public async Task Login() =>
            await HttpContext.ChallengeAsync("oidc", new AuthenticationProperties
            {
                RedirectUri = Url.Action("Index")
            });

        [Authorize]
        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc");
        }
    }
}