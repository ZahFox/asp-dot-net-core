﻿using Synomy.Models.Data;
using Microsoft.AspNetCore.Mvc;
using Synomy.Services;
using System.Threading.Tasks;
using Synomy.Models.ViewModels;
using Synomy.Models.Application;

namespace Synomy.Controllers
{
    public class LanguageController : Controller
    {
        private IFavorites favorites;
        private IHttpClient notusClient;
        private IUserService userService;

        public LanguageController(IFavorites favorites, IHttpClient notusClient, IUserService userService)
        {
            this.favorites = favorites;
            this.notusClient = notusClient;
            this.userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Index() =>
            View("List", await GetList());

        [HttpGet]
        public async Task<IActionResult> List() =>
            View(await GetList());

        private async Task<LanguageListViewModel> GetList()
        {
            var list = new LanguageListViewModel();
            var languages = await notusClient.GetDataCollection<Language>("Language");
            foreach(var language in languages)  
            {
                list.Add(new LanguageCard
                {
                    Content = ContentUtilities.CreateContent(language),
                    IsFavorited = favorites.InFavorites(language.LanguageId, "Language"),
                    Color = language.Color,
                    Icon = language.Icon
                });
            }
            return list;
        }
    }
}