﻿using Synomy.Models.Api;
using Synomy.Models.Data;
using System;

namespace Synomy.Models.Application
{
    [Serializable]
    public class Content
    {
        public string ContentType { get; set; }

        public int ContentId { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }
    }

    public static class ContentUtilities
    {
        public static Content CreateContent(Language language) =>
            new Content
            {
                ContentId = language.LanguageId,
                Title = language.Name,
                Body = language.Description,
                ContentType = language.GetType().Name
            };

        public static Content CreateContent(LanguageTopicDTO languageTopic) =>
            new Content
            {
                ContentId = languageTopic.LanguageTopicId,
                Body = languageTopic.Topic.Description,
                Title = $"{languageTopic.Language.Name}: {languageTopic.Topic.Title}",
                ContentType = "LanguageTopic"
            };

        public static Content CreateContent(Topic topic) =>
            new Content
            {
                ContentId = topic.TopicId,
                Body = topic.Description,
                Title = topic.Title,
                ContentType = topic.GetType().Name
            };

        public static Content CreateContent(Snippet snippet) =>
            new Content
            {
                ContentId = snippet.SnippetId,
                Body = snippet.Text,
                Title = snippet.Title,
                ContentType = snippet.GetType().Name
            };
    }
}
