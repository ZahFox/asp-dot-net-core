﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Application
{
    public class FavoriteCollection
    {
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a title for this collection")]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 characters.")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a description for this collection")]
        [StringLength(300, ErrorMessage = "Description cannot be longer than 300 characters.")]
        public string Description { get; set; }

        public List<Favorite> Favorites { get; set; }

        [Required(ErrorMessage = "Every Favorite Collection must belong to a User")]
        public string UserId { get; set; }
    }
}
