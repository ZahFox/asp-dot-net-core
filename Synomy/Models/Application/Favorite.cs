﻿using Synomy.Models.Api;
using Synomy.Models.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Application
{
    [Serializable]
    public class Favorite
    {
        public  int FavoriteId { get; set; }
    
        public Content Content { get; set; }

        [Required(ErrorMessage = "Every Favorite must belong to a User")]
        public string UserId { get; set; }
    }

    public static class FavoriteUtilities
    {
        public static Favorite CreateFavorite(Language language) =>
            new Favorite { Content = ContentUtilities.CreateContent(language) };

        public static Favorite CreateFavorite(LanguageTopicDTO languageTopic) =>
            new Favorite { Content = ContentUtilities.CreateContent(languageTopic) };

        public static Favorite CreateFavorite(Topic topic) =>
            new Favorite { Content = ContentUtilities.CreateContent(topic) };

        public static Favorite CreateFavorite(Snippet snippet) =>
            new Favorite { Content = ContentUtilities.CreateContent(snippet) };
    }
}
