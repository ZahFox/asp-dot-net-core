﻿using Synomy.Models.Data;
using Synomy.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synomy.Models.ViewModels
{
    public class NavMenu
    {
        public IEnumerable<NavSection> Sections { get; set; }
    }

    public class NavSection
    {
        public IEnumerable<NavItem> Items { get; set; }
        public string Label { get; set; }
        public string Title { get; set; }
    }

    public class NavItem
    {
        public string Action { get; set; }
        public string Controller { get; set; }
        public Dictionary<string, string> RouteValues { get; set; }
        public string Text { get; set; }
    }

    public static class NavMenuUtils
    {
        public static async Task<NavMenu> GetBrowseTopicNavMenu(IHttpClient notusClient)
        {
            var languages = await notusClient.GetDataCollection<Language>("Language");
            var topics = await notusClient.GetDataCollection<Topic>("Topic");

            return new NavMenu
            {
                Sections = new List<NavSection>
                {
                    new NavSection
                    {
                        Title = "Filter By Language",
                        Label = "language",
                        Items = languages.Select(language =>
                            new NavItem
                            {
                                Action = "Language",
                                Controller = "Topics",
                                RouteValues = new Dictionary<string, string>
                                {
                                    ["filter"] = language.Name,
                                },
                                Text = language.Name
                            })
                    },
                    new NavSection
                    {
                        Title = "Filter By Title",
                        Label = "title",
                        Items = topics.Select(topic =>
                            new NavItem
                            {
                                Action = "Title",
                                Controller = "Topics",
                                RouteValues = new Dictionary<string, string>
                                {
                                    ["filter"] = topic.Title,
                                },
                                Text = topic.Title
                            })
                    }
                }
            };
        }
    }
}
