﻿using System.Collections.Generic;

namespace Synomy.Models.ViewModels
{
    public enum CardType { CollectionFavorite, Favorite, Default }

    public class CardListViewModel<T>
    {
        public CardType CardType { get; set; }
        public IEnumerable<T> Data { get; set; }
        public string Controller { get; set; }
        public string FilterType { get; set; }
        public string FilterValue { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
