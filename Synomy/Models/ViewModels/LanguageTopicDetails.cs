﻿using Synomy.Models.Api;
using Synomy.Models.Data;
using System.Collections.Generic;


namespace Synomy.Models.ViewModels
{
    public class LanguageTopicDetails
    {
        public LanguageTopicDTO LanguageTopic { get; set; }

        public IEnumerable<Snippet> Snippets { get; set; }
    }
}
