﻿using System.Collections.Generic;

namespace Synomy.Models.ViewModels
{
    public class FilterSection<T>
    {
        public string Title { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string WildCard { get; set; }
        public int WildCardInt { get; set; }
        public IEnumerable<T> Filters { get; set; }
    }
}
