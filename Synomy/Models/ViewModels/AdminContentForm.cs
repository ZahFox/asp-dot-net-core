﻿using Synomy.Models.Data;

namespace Synomy.Models.ViewModels
{
    public class AdminContentForm
    {
        public string Method { get; set;}
    }

    public class AdminLanguageForm : AdminContentForm
    {
        public Language Language { get; set; }
    }

    public class AdminLanguageTopicForm : AdminContentForm
    {
        public LanguageTopic LanguageTopic { get; set; }
    }

    public class AdminTopicForm : AdminContentForm
    {
        public Topic Topic { get; set; }
    }
}
