﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synomy.Models.ViewModels
{
    public class LanguageCard : Card
    {
        public string Color { get; set; }

        public string Icon { get; set; }
    }
}
