﻿using System.Collections.Generic;

namespace Synomy.Models.ViewModels
{
    public class LanguageListViewModel
    {
        private List<LanguageCard> languageList = new List<LanguageCard>();

        public IEnumerable<LanguageCard> List => languageList;

        public void Add(LanguageCard item) => languageList.Add(item);
    }
}
