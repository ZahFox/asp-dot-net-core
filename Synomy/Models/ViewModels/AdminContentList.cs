﻿using Synomy.Models.Application;
using System.Collections.Generic;

namespace Synomy.Models.ViewModels
{
    public class AdminContentList
    {
        public IEnumerable<Content> Languages { get; set; }
        public IEnumerable<Content> Topics { get; set; }
        public IEnumerable<Content> LanguageTopics { get; set; }
        public IEnumerable<Content> Snippets { get; set; }
    }
}
