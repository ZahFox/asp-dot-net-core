﻿using Synomy.Models.Application;

namespace Synomy.Models.ViewModels
{
    public class Card
    {
        public Content Content { get; set; }

        public CardType CardType { get; set; }

        public string OptionalString1 { get; set; }

        public string OptionalString2 { get; set; }

        public int OptionalInt1 { get; set; }

        public int OptionalInt2 { get; set; }

        public bool IsFavorited { get; set; }

        public Card() { }

        public Card(Content content, bool isFavorited)
        {
            Content = content;
            IsFavorited = isFavorited;
        }
    }
}
