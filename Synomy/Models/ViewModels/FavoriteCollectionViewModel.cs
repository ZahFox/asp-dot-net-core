﻿using Synomy.Models.Application;
using System.Collections.Generic;

namespace Synomy.Models.ViewModels
{
    public class FavoriteCollectionViewModel
    {
        public Api.FavoriteCollection FavoriteCollection { get; set; }

        public CardListViewModel<Card> CardList { get; set; }
    }
}
