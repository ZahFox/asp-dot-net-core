﻿using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Data
{
    public class Topic
    {
        public int TopicId { get; set; }

        [Required(ErrorMessage = "Please include a title for this topic")]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 characters.")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a description for this topic")]
        [StringLength(500, ErrorMessage = "Description cannot be longer than 500 characters.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Every Topic must be created by a User")]
        public string UserId { get; set; }
    }
}
