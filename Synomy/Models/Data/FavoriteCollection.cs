﻿using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Data
{
    public class FavoriteCollection
    {
        public int FavoriteCollectionId { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a title for this collection")]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 characters.")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a description for this collection")]
        [StringLength(300, ErrorMessage = "Description cannot be longer than 300 characters.")]
        public string Description { get; set; }
    }
}
