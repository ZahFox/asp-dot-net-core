﻿using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Data
{
    public class Language
    {
        public int LanguageId { get; set; }

        [Required(ErrorMessage = "Please include a name for this language")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a description for this language")]
        [StringLength(300, ErrorMessage = "Description name cannot be longer than 300 characters.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please include a color for this language")]
        [StringLength(7, ErrorMessage = "Color must be a 7 character hex value that starts with #")]
        public string Color { get; set; }

        [DataType(DataType.ImageUrl)]
        [StringLength(200, ErrorMessage = "Icon url cannot be longer than 200 characters.")]
        public string Icon { get; set; }

        [Required(ErrorMessage = "Every Language must be created by a User")]
        public string UserId { get; set; }
    }
}
