﻿namespace Synomy.Models.Data
{
    public class LanguageTopic
    {
        public int LanguageTopicId { get; set; }

        // Associations //

        public int LanguageId { get; set; }

        public int TopicId { get; set; }
    }
}
