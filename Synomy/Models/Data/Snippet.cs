﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Data
{
    public class Snippet
    {
        public int SnippetId { get; set; }

        [Required(ErrorMessage = "Please include a title for this snippet")]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 characters.")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include content for this snippet")]
        [StringLength(5000, ErrorMessage = "Text cannot be longer than 5000 characters.")]
        public string Text { get; set; }

        [DataType(DataType.DateTime)]
        public DateTimeOffset Created { get; set; }

        [DataType(DataType.DateTime)]
        public DateTimeOffset LastUpdated { get; set; }

        public int LanguageTopicId { get; set; }
    }
}
