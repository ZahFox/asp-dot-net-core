﻿using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Data
{
    public class ContentType
    {
        public int ContentTypeId { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a title for this content type")]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 characters.")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a description for this content type")]
        [StringLength(300, ErrorMessage = "Description cannot be longer than 300 characters.")]
        public string Description { get; set; }
    }
}
