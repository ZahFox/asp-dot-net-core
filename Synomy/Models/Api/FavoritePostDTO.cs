﻿namespace Synomy.Models.Api
{
    public class FavoritePostDTO
    {
        public string UserId { get; set; }
        public int ContentTypeId { get; set; }
        public int ContentId { get; set; }
    }
}
