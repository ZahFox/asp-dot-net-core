﻿using Synomy.Models.Application;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Synomy.Models.Api
{
    public class FavoriteCollectionDTO
    {
        public Synomy.Models.Api.FavoriteCollection FavoriteCollection { get; set; }

        public List<Favorite> Favorites { get; set; }
    }

    public class FavoriteCollectionFavoriteDTO
    {
        public int FavoriteCollectionFavoriteId { get; set; }

        public Favorite Favorite { get; set; }
    }

    public class FavoriteCollection
    {
        public int FavoriteCollectionId { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a title for this collection")]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 characters.")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Please include a description for this collection")]
        [StringLength(300, ErrorMessage = "Description cannot be longer than 300 characters.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Every Favorite Collection must belong to a User")]
        public string UserId { get; set; }

        public List<Favorite> Favorites { get; set; }
    }
}
