﻿using Synomy.Models.Data;

namespace Synomy.Models.Api
{
    public class LanguageTopicDTO
    {
        public int LanguageTopicId { get; set; }

        public Language Language { get; set; }

        public Topic Topic { get; set; }
    }
}
